import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material.module';
import { NavigationComponent } from './navigation/navigation.component';
import { AuthModule } from '@auth0/auth0-angular';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,

    // Import the module into the application, with configuration
    AuthModule.forRoot({
      domain: 'ermes-test.eu.auth0.com',
      clientId: 'IM8ZJoPaxmasVUv6Jvp4JePBeTvSQFlh'
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
