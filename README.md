# Frontend project assignment

## Introduction
"Amazing Web Filter" is an application designed to block browser navigation towards selected websites.

The application is structured with the following components:
- Management dashboard, whose purpose is to centrally manage the website blocklist
- Software agents, installed on end-user devices, that locally enforce the website blocklist
- Backend services, which provide the set of APIs required for blocklist management and enforcement 

In the context of the "Amazing Web Filter" application, you are in charge of the design and implementation of the management dashboard. More in particular, you are asked to design a set of views that will be entitled to:
- perform user authentication against [auth0](https://auth0.com/)
- manage the website blocklist

## Requirements

### MUST-HAVE
In order to accomplish the task, you are asked to:
* create a set of mock-ups used as guideline for the real implementation (to be included as images in the "design" folder)
* create the login flow using the [auth0 angular library](https://github.com/auth0/auth0-angular) (registration is not required)
* provide the following functionalities:
    * show the blocklist
    * add a blocklist entry
    * edit a blocklist entry
    * delete a blocklist entry

### NICE-TO-HAVE
We strongly encourage the adoption of the TDD methodology:
- unit tests
- e2e tests

## Technologies
The project involves Angular as main framework, including all the tools configured by default (Typescript, linting); You can choose the UI library you prefer (e.g. Angular Material, Clarity, IBM Carbon).

## Folder structure
```
|__ design/              -> folder will contain the mockups
|__ backend/             -> folder containing the files to run the backend locally
|__ src/                 -> folder containing the frontend source files
|__ package.json         -> dependencies and scripts used to lint, test and build the frontend application, and to run the backend
```


## Auth0 configuration
To ease the development both the SPA application and the API have been created on a dedicated Auth0 tenant. Below the information required to configure the Auth0 module.
```
domain: 'ermes-test.eu.auth0.com'
clientId: 'IM8ZJoPaxmasVUv6Jvp4JePBeTvSQFlh'
audience: 'https://api.amazing.ermes'
```

The following user has been created:
```
username: amazing-user@email.com
password: amazing123!
```


## Backend
The backend provides a set of APIs by using a json file. By default, executing the command `npm run backend:start`, the file `backend/db.json` is used as database. The backend behaviour can be customized by changing the `backend/backend.js` file. For further details please refer to [json-server](https://github.com/typicode/json-server).

To provide API authorization, the backend is pre-configured to require an access token for all the routes (please have a look at [Auth0 API endpoint protection](https://auth0.com/docs/quickstart/backend/nodejs#protect-api-endpoints) for further details). Should you need it during development, it is possible to start the backend with the authorization disabled by using the command line option `--no-auth`, e.g. `npm run backend:start -- --no-auth` or `node backend/backend.js --no-auth`. Nevertheless, keep in mind that the final project submission must properly work with **authorization enabled**.

The backend exposes the `/blocklist` resource as a REST API with the following response format:
```json
[
    {
        "id": 1,
        "name": "Blocklist entry 1",
        "website": "www.acme.com"
    },
    {
        "id": 2,
        "name": "Blocklist entry 2",
        "website": "www.malicious.site"
    }
]
```
The backend supports [pagination](https://github.com/typicode/json-server#paginate), [sorting](https://github.com/typicode/json-server#sort) and [filtering](https://github.com/typicode/json-server#filter).

It provides the following operations:

```ssh
# Add an entry
POST /blocklist

{
    "name": "Blocklist entry X",
    "website": "www.malicious.xyz"
}
```

```ssh 
# Modify an entry
PATCH /blocklist/<id>

{
    "name": "Blocklist entry X",
}
```

```ssh
# Replace an entry
PUT /blocklist/<id>

{
    "name": "Blocklist entry X",
    "website": "www.malicious.xyz"
}
```

```ssh
# Delete an entry
DELETE /blocklist/<id>

{
    "name": "Blocklist entry X",
    "website": "www.malicious.xyz"
}
```

## Submission
The submission can be done by sending the bundle file generated through `git bundle create <bundle_name> master`. To verify the correctness of the bundle, it can be unbundled using `git clone -b master <bundle_name> <destination_folder>`.

**IMPORTANT**:
Keep in mind that `git` usage (logs history, commit messages, etc...) will be taken into account during the evaluation.


## Utilities and initial configuration
### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Useful links
- [Angular CLI](https://github.com/angular/angular-cli) 
- [Angular CLI Overview and Command Reference](https://angular.io/cli)
- [webpack](https://webpack.js.org/)
- [json-server](https://github.com/typicode/json-server)
- [Auth0 integration](https://auth0.com/docs/)
